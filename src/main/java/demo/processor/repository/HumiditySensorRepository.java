package demo.processor.repository;

import demo.processor.entity.HumiditySensor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Предоставляет интерфейс взаимодействия с таблицей humidity_sensor.
 */
@Repository
@Transactional(rollbackOn = Throwable.class)
public interface HumiditySensorRepository extends CrudRepository<HumiditySensor, Integer> {
}
