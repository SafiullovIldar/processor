package demo.processor.repository;

import demo.processor.entity.ContainerMonitoring;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * Предоставляет интерфейс взаимодействия с таблицей container_monitoring.
 */
@Repository
@Transactional(rollbackOn = Throwable.class)
public interface ContainerMonitoringRepository extends CrudRepository<ContainerMonitoring, Integer> {

    /**
     * @param containerId идентификатор контейнера.
     * @param sensorId идентификатор датчика.
     * @param sensorType тип датчика.
     * @return сущность таблицы container_monitoring, если совпадения по заданным параметрам найдены.
     */
    @Query("select cm \n"
        + "from ContainerMonitoring cm\n"
        + "where cm.containerId = :containerId \n"
        + "    and cm.sensorId=:sensorId \n"
        + "    and cm.sensorType=:sensorType\n")
    Optional<ContainerMonitoring> findBinding(@Param("containerId") Integer containerId,
                                              @Param("sensorId") Integer sensorId,
                                              @Param("sensorType") String sensorType);
}
