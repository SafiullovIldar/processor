package demo.processor.repository;

import demo.processor.entity.Container;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Предоставляет интерфейс взаимодействия с таблицей container.
 */
@Repository
@Transactional(rollbackOn = Throwable.class)
public interface ContainerRepository extends CrudRepository<Container, Integer> {
}
