package demo.processor.repository;

import demo.processor.entity.TemperatureSensor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Предоставляет интерфейс взаимодействия с таблицей temperature_sensor.
 */
@Repository
@Transactional(rollbackOn = Throwable.class)
public interface TemperatureSensorRepository extends CrudRepository<TemperatureSensor, Integer> {
}
