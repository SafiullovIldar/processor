package demo.processor.repository;

import demo.processor.entity.ShockSensor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Предоставляет интерфейс взаимодействия с таблицей shock_sensor.
 */
@Repository
@Transactional(rollbackOn = Throwable.class)
public interface ShockSensorRepository extends CrudRepository<ShockSensor, Integer> {
}
