package demo.processor.entity;

import demo.processor.entity.interfaces.SensorInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Сущность таблицы humidity_sensor. */
@Setter
@Getter
@Entity
@Table(name = "humidity_sensor")
public class HumiditySensor implements SensorInfo {

    /**
     * Идентификатор датчика влажности. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Показание датчика влажности. */
    @Column(name = "humidity")
    private Integer humidity;

    @Override
    public String getValue() {
        return humidity.toString();
    }
}
