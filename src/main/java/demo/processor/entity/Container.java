package demo.processor.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Сущность таблицы  container.
 */
@Setter
@Getter
@Entity
@Table(name = "container")
public class Container {

    /**
     * Идентификатор контейнера. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Датчик температуры. */
    @JoinColumn(name = "temperature_sensor_id")
    @OneToOne(fetch = FetchType.LAZY)
    private TemperatureSensor temperatureSensor;

    /**
     * Датчик удара. */
    @JoinColumn(name = "shock_sensor_id")
    @OneToOne(fetch = FetchType.LAZY)
    private ShockSensor shockSensor;

    /**
     * Датчик влажности. */
    @JoinColumn(name = "humidity_sensor_id")
    @OneToOne(fetch = FetchType.LAZY)
    private HumiditySensor humiditySensor;
}
