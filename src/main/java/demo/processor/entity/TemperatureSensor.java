package demo.processor.entity;

import demo.processor.entity.interfaces.SensorInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Сущность таблицы temperature_sensor. */
@Getter
@Setter
@Entity
@Table(name = "temperature_sensor")
public class TemperatureSensor implements SensorInfo {

    /**
     * Идентификатор датчика температуры. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Показанеи датчика температуры. */
    @Column(name = "temperature")
    private Double temperature;

    @Override
    public String getValue() {
        return temperature.toString();
    }
}
