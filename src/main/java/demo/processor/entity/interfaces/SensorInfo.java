package demo.processor.entity.interfaces;

/**
 * Общий интерфейс для всех датчиков.
 */
public interface SensorInfo {

    /**
     * @return идентификатор датчика. */
    Integer getId();

    /**
     * @return строкое представление показания датчика. */
    String getValue();
}
