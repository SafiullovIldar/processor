package demo.processor.entity;

import demo.processor.entity.interfaces.SensorInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Сущность таблицы shock_sensor. */
@Setter
@Getter
@Entity
@Table(name = "shock_sensor")
public class ShockSensor implements SensorInfo {

    /**
     * Идентификатор датчика удара. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Информация о повреждении. */
    @Column(name = "is_damaged")
    private Boolean isDamaged;

    @Override
    public String getValue() {
        return isDamaged.toString();
    }
}
