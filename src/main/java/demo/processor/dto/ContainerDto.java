package demo.processor.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import demo.processor.dto.enums.SensorType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Дто с информацией о контейнере.
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ContainerDto {

    /**
     * Идентификатор контейнера. */
    @JsonProperty("id")
    private Integer id;

    /**
     * Датчик температуры. */
    @JsonProperty("temperature_sensor")
    private TemperatureSensorDto temperatureSensorDto;

    /**
     * Датчик удара. */
    @JsonProperty("shock_sensor")
    private ShockSensorDto shockSensorDto;

    /**
     * Датчик влажности. */
    @JsonProperty("humidity_sensor")
    private HumiditySensorDto humiditySensorDto;

    /**
     * Тип датчика. */
    @JsonProperty("sensor_sensor")
    private SensorType sensorType;

    public ContainerDto(Integer id, TemperatureSensorDto temperatureSensorDto, SensorType sensorType) {
        this.id = id;
        this.temperatureSensorDto = temperatureSensorDto;
        this.sensorType = sensorType;
    }

    public ContainerDto(Integer id, ShockSensorDto shockSensorSto, SensorType sensorType) {
        this.id = id;
        this.shockSensorDto = shockSensorSto;
        this.sensorType = sensorType;
    }

    public ContainerDto(Integer id, HumiditySensorDto humiditySensorDto, SensorType sensorType) {
        this.id = id;
        this.humiditySensorDto = humiditySensorDto;
        this.sensorType = sensorType;
    }
}
