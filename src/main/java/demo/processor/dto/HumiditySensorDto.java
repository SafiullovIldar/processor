package demo.processor.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Дто с информацией о датчике влажности.
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class HumiditySensorDto {

    /**
     * Идентификатор датчика влажности. */
    @JsonProperty("id")
    private Integer id;

    /**
     * Показания датчика (Влажность). */
    @JsonProperty("humidity")
    private Integer humidity;
}
