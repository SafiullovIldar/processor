package demo.processor.services;

import demo.processor.entity.Container;
import demo.processor.entity.interfaces.SensorInfo;

/**
 * Сервис для взаимодествия с сущностью {@link Container}.
 */
public interface ContainerService {

    /**
     * @param id идентификатор контейнера.
     * @param sensorInfo информация о датчике.
     * @return сущность {@link Container}.
     */
    Container saveContainer(Integer id, SensorInfo sensorInfo);
}
