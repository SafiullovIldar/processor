package demo.processor.services;

import demo.processor.dto.ContainerDto;
import demo.processor.entity.Container;

/**
 * Сервис для взаимодействия с kafka.
 */
public interface KafkaService {

    /**
     * @param containerDto дто с информацией о контейнере.
     * @return сохроняет полученную информацию о контейнере в бд.
     */
    Container saveNewInformationAboutContainer(ContainerDto containerDto);
}
