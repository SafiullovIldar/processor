package demo.processor.services;

import demo.processor.entity.ShockSensor;
import demo.processor.dto.ContainerDto;

/**
 * Сервис для взаимодествия с сущностью {@link ShockSensor}.
 */
public interface ShockSensorService {

    /**
     * @param containerDto дто с информацией о контейнере.
     * @return сущность {@link ShockSensor}.
     */
    ShockSensor saveShockSensor(ContainerDto containerDto);
}
