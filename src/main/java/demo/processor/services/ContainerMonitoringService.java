package demo.processor.services;

import demo.processor.entity.Container;
import demo.processor.entity.ContainerMonitoring;
import demo.processor.entity.interfaces.SensorInfo;
import demo.processor.dto.enums.SensorType;

/**
 * Сервис для взаимодествия с сущностью {@link ContainerMonitoring}.
 */
public interface ContainerMonitoringService {

    /**
     * @param sensorInfo информация о датчике.
     * @param container контейнер.
     * @param sensorType тип датчика.
     * @return сущность {@link ContainerMonitoring}.
     */
    ContainerMonitoring saveContainerMonitoring(SensorInfo sensorInfo,
                                                Container container,
                                                SensorType sensorType);
}
