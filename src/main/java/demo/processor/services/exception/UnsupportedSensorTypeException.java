package demo.processor.services.exception;

import lombok.NonNull;

/**
 * Бросается в случае получения типа датчика, не соотвествующий ни одному из заранее известных.
 */
public class UnsupportedSensorTypeException extends RuntimeException {

    /**
     * @param message сообщение об ошибке.
     */
    public UnsupportedSensorTypeException(@NonNull final String message) {
        super(message);
    }
}
