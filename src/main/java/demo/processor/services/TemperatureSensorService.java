package demo.processor.services;

import demo.processor.entity.TemperatureSensor;
import demo.processor.dto.ContainerDto;

/**
 * Сервис для взаимодествия с сущностью {@link TemperatureSensor}.
 */
public interface TemperatureSensorService {

    /**
     * @param containerDto дто с информацией о контейнере.
     * @return сущность {@link TemperatureSensor}.
     */
    TemperatureSensor saveTemperatureSensor(ContainerDto containerDto);
}
