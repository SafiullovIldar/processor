package demo.processor.services;

import demo.processor.dto.ContainerDto;
import demo.processor.entity.HumiditySensor;

/**
 * Сервис для взаимодествия с сущностью {@link HumiditySensor}.
 */
public interface HumiditySensorService {

    /**
     * @param containerDto дто с информацией о контейнере.
     * @return сущность {@link HumiditySensor}.
     */
    HumiditySensor saveHumiditySensor(ContainerDto containerDto);
}
