package demo.processor.services.impl;

import demo.processor.dto.ContainerDto;
import demo.processor.dto.HumiditySensorDto;
import demo.processor.entity.HumiditySensor;
import demo.processor.repository.HumiditySensorRepository;
import demo.processor.services.HumiditySensorService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

/**
 * Сервис для взаимодествия с сущностью {@link HumiditySensor}.
 */
@RequiredArgsConstructor
@Service
public class HumiditySensorServiceImpl implements HumiditySensorService {

    /**
     * Интерфейс взаимодействия с таблицей humidity_sensor. */
    @NonNull private final HumiditySensorRepository humiditySensorRepository;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public HumiditySensor saveHumiditySensor(@NonNull final ContainerDto containerDto) {
        final HumiditySensorDto humiditySensorDto = containerDto.getHumiditySensorDto();
        final HumiditySensor humiditySensor = getHumiditySensor(humiditySensorDto);
        final HumiditySensor savedHumiditySensor = humiditySensorRepository.save(humiditySensor);

        return savedHumiditySensor;
    }

    /**
     * @param humiditySensorDto дто с информацией о датчике влажности.
     * @return сущность {@link HumiditySensor}.
     * @throws EntityNotFoundException в случае, если сущность с заданным идентификатором не найдена.
     */
    private HumiditySensor getHumiditySensor(@NonNull final HumiditySensorDto humiditySensorDto) {
        final Integer sensorId = humiditySensorDto.getId();
        final Optional<HumiditySensor> humiditySensorOptional = humiditySensorRepository.findById(sensorId);

        if (!humiditySensorOptional.isPresent()) {
            final String exceptionMessage = "Датчик влажности с идентификатором [%d] не найден.";
            throw new EntityNotFoundException(String.format(exceptionMessage, sensorId));
        }

        final HumiditySensor humiditySensor = humiditySensorOptional.get();
        humiditySensor.setHumidity(humiditySensorDto.getHumidity());

        return humiditySensor;
    }
}
