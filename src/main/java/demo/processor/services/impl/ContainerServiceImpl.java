package demo.processor.services.impl;

import demo.processor.entity.Container;
import demo.processor.entity.HumiditySensor;
import demo.processor.entity.ShockSensor;
import demo.processor.entity.TemperatureSensor;
import demo.processor.entity.interfaces.SensorInfo;
import demo.processor.repository.ContainerRepository;
import demo.processor.services.ContainerService;
import demo.processor.services.exception.UnsupportedSensorTypeException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

/**
 * Сервис для взаимодествия с сущностью {@link Container}.
 */
@Service
@RequiredArgsConstructor
public class ContainerServiceImpl implements ContainerService {

    /**
     * Интерфейс взаимодействия с таблицей container. */
    @NonNull private final ContainerRepository containerRepository;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Container saveContainer(@NonNull final Integer id, @NonNull final SensorInfo sensorInfo) {
        final Container cont = getContainerEntity(id);
        handleSensorType(sensorInfo, cont);
        final Container savedContainer = containerRepository.save(cont);

        return savedContainer;
    }

    /**
     * Достаёт из базы контейнер с указанным идентификатор, если таковой нет, бросает исключение.
     * @param containerId идентификатор контейнера.
     * @return сущность {@link Container}.
     * @throws EntityNotFoundException в случае, если сущность с указанным идентификатор отстуствует в бд.
     */
    private Container getContainerEntity(@NonNull final Integer containerId) {
        final Optional<Container> containerEntityOptional = containerRepository.findById(containerId);

        if (!containerEntityOptional.isPresent()) {
            final String exceptionMessage = "Контейнер с идентификатором [%d] не найден.";
            throw new EntityNotFoundException(String.format(exceptionMessage, containerId));
        }

        return containerEntityOptional.get();
    }

    /**
     * Присваивает значение сущности {@link Container} в соотвествии с типом датчика.
     * @param sensorInfo информация о датчике.
     * @param container сущность {@link Container}.
     */
    private void handleSensorType(@NonNull final SensorInfo sensorInfo, @NonNull final  Container container) {
        if (sensorInfo instanceof TemperatureSensor) {
            container.setTemperatureSensor((TemperatureSensor) sensorInfo);
        }

        else if (sensorInfo instanceof ShockSensor) {
            container.setShockSensor((ShockSensor) sensorInfo);
        }

        else if (sensorInfo instanceof HumiditySensor) {
            container.setHumiditySensor((HumiditySensor) sensorInfo);
        }

        else throw new UnsupportedSensorTypeException("Не поддерживаемый тип датчика.");
    }
}
