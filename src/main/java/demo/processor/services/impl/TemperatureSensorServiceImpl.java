package demo.processor.services.impl;

import demo.processor.entity.TemperatureSensor;
import demo.processor.dto.ContainerDto;
import demo.processor.dto.TemperatureSensorDto;
import demo.processor.repository.TemperatureSensorRepository;
import demo.processor.services.TemperatureSensorService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

/**
 * Сервис для взаимодествия с сущностью {@link TemperatureSensor}.
 */
@Service
@RequiredArgsConstructor
public class TemperatureSensorServiceImpl implements TemperatureSensorService {

    /**
     * Интерфейс взаимодействия с таблицей temperature_sensor. */
    @NonNull private final TemperatureSensorRepository temperatureSensorRepository;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public TemperatureSensor saveTemperatureSensor(@NonNull final ContainerDto containerDto) {
        final TemperatureSensorDto temperatureSensorDto = containerDto.getTemperatureSensorDto();
        final TemperatureSensor temperatureSensor = getTemperatureSensor(temperatureSensorDto);
        final TemperatureSensor savedTemperatureSensor = temperatureSensorRepository.save(temperatureSensor);

        return savedTemperatureSensor;
    }

    /**
     * @param temperatureSensorDto дто с информацией о датчике температуры.
     * @return сущность {@link TemperatureSensor}.
     * @throws EntityNotFoundException в случае, если сущность с заданным идентификатором не найдена.
     */
    private TemperatureSensor getTemperatureSensor(@NonNull final TemperatureSensorDto temperatureSensorDto) {
        final Integer sensorId = temperatureSensorDto.getId();
        final Optional<TemperatureSensor> temperatureSensorOptional =
            temperatureSensorRepository.findById(temperatureSensorDto.getId());

        if (!temperatureSensorOptional.isPresent()) {
            final String exceptionMessage = "Датчик температуры с идентификатором [%d] не найден.";
            throw new EntityNotFoundException(String.format(exceptionMessage, sensorId));
        }

        final TemperatureSensor temperatureSensor = temperatureSensorOptional.get();
        temperatureSensor.setTemperature(temperatureSensorDto.getTemperature());

        return temperatureSensor;
    }
}
