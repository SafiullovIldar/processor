package demo.processor.services.impl;

import demo.processor.dto.ContainerDto;
import demo.processor.dto.enums.SensorType;
import demo.processor.entity.Container;
import demo.processor.entity.HumiditySensor;
import demo.processor.entity.ShockSensor;
import demo.processor.entity.TemperatureSensor;
import demo.processor.services.ContainerMonitoringService;
import demo.processor.services.ContainerService;
import demo.processor.services.HumiditySensorService;
import demo.processor.services.KafkaService;
import demo.processor.services.ShockSensorService;
import demo.processor.services.TemperatureSensorService;
import demo.processor.services.exception.UnsupportedSensorTypeException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Сервис для взаимодействия с kafka.
 */
@Service
@RequiredArgsConstructor
public class KafkaServiceImpl implements KafkaService {

    /**
     * Сервис для взаимодествия с сущностью {@link Container}. */
    @NonNull private final ContainerService containerService;

    /**
     * Сервис для взаимодествия с сущностью {@link demo.processor.entity.ContainerMonitoring}. */
    @NonNull private final ContainerMonitoringService containerMonitoringService;

    /**
     * Сервис для взаимодествия с сущностью {@link TemperatureSensor}. */
    @NonNull private final TemperatureSensorService temperatureSensorService;

    /**
     * Сервис для взаимодествия с сущностью {@link ShockSensor}. */
    @NonNull private final ShockSensorService shockSensorService;

    /**
     * Сервис для взаимодествия с сущностью {@link HumiditySensor}. */
    @NonNull private final HumiditySensorService humiditySensorService;

    /**
     * Выводит информацию в лог. */
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaServiceImpl.class);

    /**
     * Получает информацию из kafka и записывает в бд.
     * @param containerDto дто с информацией о контейнере.
     * @return сущность {@link Container}.
     */
    @KafkaListener(topics = "${topic.name.sensors}", groupId = "${topic.sensors.groupId}")
    public Container listenSensorsTopic(@NonNull final ContainerDto containerDto) {
        LOGGER.info("Получено из kafka " + containerDto);
        return saveNewInformationAboutContainer(containerDto);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Container saveNewInformationAboutContainer(@NonNull final ContainerDto containerDto) {
        final SensorType type = containerDto.getSensorType();
        final Container savedContainer;
        switch (type) {
            case TEMPERATURE:
                final TemperatureSensor temperatureSensor =
                    temperatureSensorService.saveTemperatureSensor(containerDto);
                savedContainer = containerService.saveContainer(containerDto.getId(), temperatureSensor);
                containerMonitoringService.saveContainerMonitoring(temperatureSensor, savedContainer, type);
                break;
            case SHOCK:
                final ShockSensor shockSensor = shockSensorService.saveShockSensor(containerDto);
                savedContainer = containerService.saveContainer(containerDto.getId(), shockSensor);
                containerMonitoringService.saveContainerMonitoring(shockSensor, savedContainer, type);
                break;
            case HUMIDITY:
                final HumiditySensor humiditySensor = humiditySensorService.saveHumiditySensor(containerDto);
                savedContainer = containerService.saveContainer(containerDto.getId(), humiditySensor);
                containerMonitoringService.saveContainerMonitoring(humiditySensor, savedContainer, type);
                break;
            default: throw new UnsupportedSensorTypeException("Не поддерживаемый тип датчика.");
        }

        return savedContainer;
    }
}
