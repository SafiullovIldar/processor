package demo.processor.services.impl;

import demo.processor.entity.ShockSensor;
import demo.processor.dto.ContainerDto;
import demo.processor.dto.ShockSensorDto;
import demo.processor.repository.ShockSensorRepository;
import demo.processor.services.ShockSensorService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

/**
 * Сервис для взаимодествия с сущностью {@link ShockSensor}.
 */
@RequiredArgsConstructor
@Service
public class ShockSensorServiceImpl implements ShockSensorService {

    /**
     * Интерфейс взаимодействия с таблицей shock_sensor. */
    @NonNull private final ShockSensorRepository shockSensorRepository;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ShockSensor saveShockSensor(@NonNull final ContainerDto containerDto) {
        final ShockSensorDto shockSensorDto = containerDto.getShockSensorDto();
        final ShockSensor shockSensor = getShockSensor(shockSensorDto);
        final ShockSensor savedShockSensor = shockSensorRepository.save(shockSensor);

        return savedShockSensor;
    }

    /**
     * @param shockSensorDto дто с информацией о датчике удара.
     * @return сущность {@link ShockSensor}.
     * @throws EntityNotFoundException в случае, если сущность с заданным идентификатором не найдена.
     */
    private ShockSensor getShockSensor(@NonNull final ShockSensorDto shockSensorDto) {
        final Integer sensorId = shockSensorDto.getId();
        final Optional<ShockSensor> shockSensorOptional = shockSensorRepository.findById(shockSensorDto.getId());

        if (!shockSensorOptional.isPresent()) {
            final String exceptionMessage = "Датчик удара с идентификатором [%d] не найден.";
            throw new EntityNotFoundException(String.format(exceptionMessage, sensorId));
        }

        final ShockSensor shockSensor = shockSensorOptional.get();
        shockSensor.setIsDamaged(shockSensorDto.getIsDamaged());

        return shockSensor;
    }
}
