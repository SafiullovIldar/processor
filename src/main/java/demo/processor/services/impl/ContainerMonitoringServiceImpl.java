package demo.processor.services.impl;

import demo.processor.entity.Container;
import demo.processor.entity.ContainerMonitoring;
import demo.processor.entity.interfaces.SensorInfo;
import demo.processor.dto.enums.SensorType;
import demo.processor.repository.ContainerMonitoringRepository;
import demo.processor.services.ContainerMonitoringService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Сервис для взаимодествия с сущностью {@link ContainerMonitoring}.
 */
@Service
@RequiredArgsConstructor
public class ContainerMonitoringServiceImpl implements ContainerMonitoringService {

    /**
     * Интерфейс взаимодействия с таблицей container_monitoring. */
    @NonNull private final ContainerMonitoringRepository containerMonitoringRepository;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ContainerMonitoring saveContainerMonitoring(@NonNull final SensorInfo sensorInfo,
                                                       @NonNull final Container container,
                                                       @NonNull final SensorType sensorType)
    {
        final ContainerMonitoring containerMonitoring =
            getContainerMonitoringBinding(container.getId(), sensorInfo.getId(), sensorType.getValue());
        containerMonitoring.setContainerId(container.getId());
        containerMonitoring.setSensorId(sensorInfo.getId());
        containerMonitoring.setSensorType(sensorType.getValue());
        containerMonitoring.setIndication(sensorInfo.getValue());
        final ContainerMonitoring savedContainerMonitoring = containerMonitoringRepository.save(containerMonitoring);

        return savedContainerMonitoring;
    }

    /**
     * Достаёт из базы сущность {@link ContainerMonitoring}, если по указанным параметром ничего не найдено,
     * создаёт новую.
     * @param containerId идентификатор контейнера.
     * @param sensorId идентификатор датчика.
     * @param sensorType тип датчика.
     * @return сущность {@link ContainerMonitoring}.
     */
    private ContainerMonitoring getContainerMonitoringBinding(@NonNull final Integer containerId,
                                                              @NonNull final Integer sensorId,
                                                              @NonNull final String sensorType)
    {
        final Optional<ContainerMonitoring> binding =
            containerMonitoringRepository.findBinding(containerId, sensorId, sensorType);
        final ContainerMonitoring containerMonitoring = binding.orElseGet(ContainerMonitoring::new);

        return containerMonitoring;
    }
}
